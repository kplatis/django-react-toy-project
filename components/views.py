from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from oauth2_provider.views.generic import ProtectedResourceView
from django.http import HttpResponse
from components.models import Component
from components.serializers import ComponentSerializer
from rest_framework import generics, permissions


# Create your views here.

class ComponentListCreate(generics.ListCreateAPIView):
    queryset = Component.objects.all()
    serializer_class = ComponentSerializer
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ComponentListCreate, self).dispatch(request, *args, **kwargs)


class ApiEndpoint(ProtectedResourceView):

    def get(self, request, *args, **kwargs):
        return HttpResponse('Hello, OAuth2!')
