from django.contrib import admin
from components.models import Component, Link, LinkBox

# Register your models here.
admin.site.register(Component)
admin.site.register(LinkBox)
admin.site.register(Link)
