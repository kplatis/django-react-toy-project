from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Component(models.Model):
    title = models.CharField(max_length=100)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "%s of %s" % (self.title, self.owner)


class LinkBox(Component):

    def __str__(self):
        return super().__str__()


class Link(models.Model):
    link_box = models.ForeignKey(LinkBox,
                                 on_delete=models.CASCADE,
                                 related_name="links",
                                 related_query_name="link")
    link = models.URLField(max_length=100)
    title = models.CharField(max_length=300)
    new_page = models.BooleanField()