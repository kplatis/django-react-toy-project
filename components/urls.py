from django.conf.urls import url
from django.urls import path

from components.views import ApiEndpoint
from . import views

urlpatterns = [
    url(r'^api/components', views.ComponentListCreate.as_view()),
    url(r'^api/hello', ApiEndpoint.as_view()),  # an example resource endpoint

]